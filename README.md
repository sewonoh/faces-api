# com.canary.faces:faces-api:1.0-SNAPSHOT

## Dependencies

### Build-Time

- JDK 8
- Maven
- Docker - it also requires environment variable _DOCKER_EXE_ to be pointing to
  Docker executable

### Run-Time

- JRE 8
- Docker

## Building

	git clone git@bitbucket.org:sewonoh/faces-api.git
	cd faces-api
	mvn clean install -Dintegration-test

## Running

### Spring Boot application

- To run Spring Boot executable jar file:

	java -jar target/faces-api-1.0-SNAPSHOT.jar
	
### Docker container

- To create Docker container:

	docker create -p 8080:8080 --name <container name> canary-faces-api:1.0-SNAPSHOT
	
- To run Docker container:

	docker start <container name>
	
- To stop Docker container:

	docker stop <container name>
	
- To remove Docker container:

	docker rm <container name>
	
- To remove the Docker image:

	docker rmi canary-faces-api:1.0-SNAPSHOT
	
### Swagger2

- Run the application
- Open web browser and navigate to http://localhost:8080/swagger-ui.html