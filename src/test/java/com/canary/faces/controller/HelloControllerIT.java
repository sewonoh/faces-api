package com.canary.faces.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = { HelloController.class })
@AutoConfigureMockMvc
@PropertySource("classpath:application-it.properties")
public class HelloControllerIT {

	  /**
	   * TCP/IP port on which server instance is run.
	   */
	  @LocalServerPort
	  private int              port;

	  /**
	   * Base URL for servlet instance.
	   */
	  private URL              base;

	  /**
	   * REST template for integration tests.
	   */
	  @Autowired
	  private TestRestTemplate template;
	  
	  @Before
	  public void setUp() throws Exception {
		  
		  this.base = new URL("http://localhost:" + this.port + "/hello");
		  
	  }
	  
	  @Test
	  public void indexIT() {
		  
		  final ResponseEntity<String> response = this.template
				  .getForEntity(base.toString(), String.class);
		  
		  assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		  assertThat(response.getBody(), equalTo("Saying hi"));
		  
	  }
}
