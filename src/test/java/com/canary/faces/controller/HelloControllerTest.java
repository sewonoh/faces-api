package com.canary.faces.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit tests for HelloController.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { HelloController.class })
@AutoConfigureMockMvc
@PropertySource("classpath:application-test.properties")
public class HelloControllerTest {

	@Autowired
	HelloController controller;
	
	@Test
	public void indexText() {
		
		ResponseEntity<String> response = this.controller.index();
		
		assertEquals(response.getBody(), "Saying hi");
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		
	}
}
