package com.canary.faces.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2 {
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("faces-api")
				.select()
				.apis(RequestHandlerSelectors
						.basePackage("com.canary.faces.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
				"Faces API",
				"Lorem Ipsum is simply dummy text of the printing and typesetting "
						+ "industry. Lorem Ipsum has been the industry's standard dummy text "
						+ "ever since the 1500s, when an unknown printer took a galley of "
						+ "type and scrambled it to make a type specimen book. It has survived "
						+ "not only five centuries, but also the leap into electronic "
						+ "typesetting, remaining essentially unchanged. It was popularised in "
						+ "the 1960s with the release of Letraset sheets containing Lorem Ipsum "
						+ "passages, and more recently with desktop publishing software like "
						+ "Aldus PageMaker including versions of Lorem Ipsum.",
						"1.0",
						"Terms of service",
						new Contact("Admin", "http://localhost", "admin@localhost"),
						"Apache License Version 2.0", 
						"https://www.apache.org/licenses/LICENSE-2.0");
	}

	@Bean
	UiConfiguration uiConfig() {
		return new UiConfiguration(
				null,
				"none",
				"alpha",
				"schema",
				UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
				false,
				true,
				60000L);
	}
}
