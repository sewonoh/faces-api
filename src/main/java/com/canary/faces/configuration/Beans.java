package com.canary.faces.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

@Configuration
public class Beans {

	  @Value("${cloud.aws.region.static}")
	  private String region;
	  
	  @Value("${cloud.aws.credentials.accessKey}")
	  private String accessKey;

	  @Value("${cloud.aws.credentials.secretKey}")
	  private String secretKey;
	  
	  private AWSCredentials credentials() {
		  
		  return new BasicAWSCredentials(accessKey, secretKey);
		  
	  }
	  
	  private AWSCredentialsProvider provider() {
		  
		  return new AWSStaticCredentialsProvider(credentials());
		  
	  }
	  
	  private ClientConfiguration clientConfiguration() {
		  
		  ClientConfiguration clientConfig = new ClientConfiguration();
		  clientConfig.setConnectionTimeout(30000);
		  clientConfig.setRequestTimeout(60000);
		  clientConfig.setProtocol(Protocol.HTTPS);
		  
		  return clientConfig;
	  }
	  
	  @Bean
	  public AmazonS3 amazonS3() {
		  
		  return AmazonS3Client.builder()
				    .withRegion(region)
				    .withCredentials(provider())
				    .build();
		  
	  }
	  
	  @Bean
	  public AmazonRekognition rekognitionClient() {
		  
		  return AmazonRekognitionClientBuilder.standard()
				  .withClientConfiguration(clientConfiguration())
				  .withCredentials(provider())
				  .withRegion(region)
				  .build();
		  
	  }
	
}
