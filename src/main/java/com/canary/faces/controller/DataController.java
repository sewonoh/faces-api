package com.canary.faces.controller;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
public class DataController {

    private final String BUCKET = "pjeongamplifyproject8e3ab220f6f54a7ea9efecf874b92828";

    private final String LEO_REF = "public/referenceImages/leo-ref.jpg";
    private final String LEVIN_REF = "public/referenceImages/levin-ref.jpg";

    @Autowired
    private AmazonRekognition rekognitionClient;

    // public/referenceImages/D52356AF-2AC2-49CC-9AC8-E9595241D640.jpg
    @RequestMapping("/detectLabels")
    public String detectLabels() {
        String photo = "public/referenceImages/D52356AF-2AC2-49CC-9AC8-E9595241D640.jpg";
        String bucket = "pjeongamplifyproject8e3ab220f6f54a7ea9efecf874b92828";

        DetectLabelsRequest request = new DetectLabelsRequest()
                .withImage(new Image()
                        .withS3Object(new S3Object()
                                .withName(photo).withBucket(bucket)))
                .withMaxLabels(10)
                .withMinConfidence(75F);

        try {
            DetectLabelsResult result = rekognitionClient.detectLabels(request);
            List<Label> labels = result.getLabels();

            System.out.println("Detected labels for " + photo);
            for (Label label : labels) {
                System.out.println(label.getName() + ": " + label.getConfidence().toString());
            }
        } catch (AmazonRekognitionException e) {
            e.printStackTrace();
        }

        return "success";
    }

    /**
     * Create a Collection, this would be where the reference images for each class would be stored
     *
     * @return
     */
    @PostMapping("/collections")
    public ResponseEntity<?> createCollection(@RequestBody Map<String, Object> collectionRequestDetails) {
        String collectionId = (String) collectionRequestDetails.get("collectionId");

        System.out.println("Creating collection: " + collectionId);

        CreateCollectionRequest request = new CreateCollectionRequest().withCollectionId(collectionId);

        CreateCollectionResult createCollectionResult = rekognitionClient.createCollection(request);

        System.out.println("CollectionArn : " + createCollectionResult.getCollectionArn());
        System.out.println("Status code : " + createCollectionResult.getStatusCode().toString());

        //aws:rekognition:us-east-1:983966203444:collection/newCollection
        return new ResponseEntity<>(createCollectionResult.getCollectionArn(), HttpStatus.OK);
    }


    /**
     * List Collections, this would be where the reference images for each class would be stored
     *
     * @return
     */
    @GetMapping("/collections")
    public ResponseEntity<?> listCollections() {
        System.out.println("Listing collections");
        int limit = 10;
        ListCollectionsResult listCollectionsResult = null;
        List<String> collectionIds;
        String paginationToken = null;
        do {
            if (listCollectionsResult != null) {
                paginationToken = listCollectionsResult.getNextToken();
            }
            ListCollectionsRequest listCollectionsRequest = new ListCollectionsRequest().withMaxResults(limit).withNextToken(paginationToken);
            listCollectionsResult = rekognitionClient.listCollections(listCollectionsRequest);

            collectionIds = listCollectionsResult.getCollectionIds();
            for (String resultId : collectionIds) {
                System.out.println(resultId);
            }
        } while (listCollectionsResult != null && listCollectionsResult.getNextToken() != null);

        return new ResponseEntity<>(collectionIds, HttpStatus.OK);
    }

    @DeleteMapping("/collections/{collectionId}")
    public ResponseEntity<?> deleteCollection(@PathVariable("collectionId") String collectionId) {
        System.out.println("Deleting collection: " + collectionId);

        DeleteCollectionRequest request = new DeleteCollectionRequest().withCollectionId(collectionId);
        DeleteCollectionResult deleteCollectionResult = rekognitionClient.deleteCollection(request);

        System.out.println(collectionId + ": " + deleteCollectionResult.getStatusCode().toString());

        return new ResponseEntity<>(collectionId, HttpStatus.OK);
    }

    @GetMapping("/collections/{collectionId}")
    public ResponseEntity<?> describeCollection(@PathVariable("collectionId") String collectionId) {

        System.out.println("Describing collection: " + collectionId );

        DescribeCollectionRequest request = new DescribeCollectionRequest().withCollectionId(collectionId);

        DescribeCollectionResult describeCollectionResult = rekognitionClient.describeCollection(request);
        System.out.println("Collection Arn : " + describeCollectionResult.getCollectionARN());
        System.out.println("Face count : " + describeCollectionResult.getFaceCount().toString());
        System.out.println("Face model version : " + describeCollectionResult.getFaceModelVersion());
        System.out.println("Created : " + describeCollectionResult.getCreationTimestamp().toString());

        return new ResponseEntity<>(describeCollectionResult, HttpStatus.OK);
    }

    @PostMapping("/collections/{collectionId}/faces")
    public ResponseEntity<?> addFacesToCollection(@PathVariable("collectionId") String collectionId, @RequestBody Map<String, Object> requestMap) {
        String photoKey = (String) requestMap.get("photo");
        String externalImageId = (String) requestMap.get("externalImageId");

        if (photoKey == null || photoKey.length() == 0) {
            photoKey = LEO_REF;
        }

        if (externalImageId == null || externalImageId.length() == 0) {
            externalImageId = "defaultExtImageId";
        }

        Image image = new Image()
                .withS3Object(new S3Object()
                        .withBucket(BUCKET)
                        .withName(photoKey));

        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withCollectionId(collectionId)
                .withExternalImageId(externalImageId)
                .withDetectionAttributes("DEFAULT");

        IndexFacesResult indexFacesResult = rekognitionClient.indexFaces(indexFacesRequest);

        System.out.println("Results for " + photoKey);
        System.out.println("Faces indexed:");

        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
        for (FaceRecord faceRecord : faceRecords) {
            System.out.println("  Face ID: " + faceRecord.getFace().getFaceId());
            System.out.println("  Location:" + faceRecord.getFaceDetail().getBoundingBox().toString());
        }

//        For Leo_ref:
//        Faces indexed:
//        Face ID: a11f49d3-ba58-4b3c-906c-32cf24edc060
//        Location:{Width: 0.47666666,Height: 0.3575,Left: 0.24,Top: 0.316875}

        // sample result:
//        Results for public/referenceImages/D52356AF-2AC2-49CC-9AC8-E9595241D640.jpg
//        Faces indexed:
//        Face ID: 47795010-cfab-42a8-9700-019da6665544
//        Location:{Width: 0.5124777,Height: 0.51111114,Left: 0.2228164,Top: 0.29955557}

        return new ResponseEntity<>("success", HttpStatus.OK);
    }


    @GetMapping("/collections/{collectionId}/faces")
    public ResponseEntity<?> listFacesInCollection(@PathVariable("collectionId") String collectionId) {
        ObjectMapper objectMapper = new ObjectMapper();

        ListFacesResult listFacesResult = null;
        System.out.println("Faces in collection " + collectionId);

        List<Face> faces;
        String paginationToken = null;
        do {
            if (listFacesResult != null) {
                paginationToken = listFacesResult.getNextToken();
            }

            ListFacesRequest listFacesRequest = new ListFacesRequest()
                    .withCollectionId(collectionId)
                    .withMaxResults(1)
                    .withNextToken(paginationToken);

            listFacesResult = rekognitionClient.listFaces(listFacesRequest);
            faces = listFacesResult.getFaces();
            for (Face face : faces) {
                try {
                    System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        } while (listFacesResult != null && listFacesResult.getNextToken() != null);

//        Faces in collection testCollection
//        {
//            "faceId" : "a11f49d3-ba58-4b3c-906c-32cf24edc060",
//                "boundingBox" : {
//            "width" : 0.476667,
//                    "height" : 0.3575,
//                    "left" : 0.24,
//                    "top" : 0.316875
//        },
//            "imageId" : "a2692ee5-8ead-52ab-b5c9-c9ed48c11216",
//                "externalImageId" : "LeoJeong-studentId",
//                "confidence" : 99.901596
//        }
//        {
//            "faceId" : "fd813c43-f57d-4501-a043-5ac6648be932",
//                "boundingBox" : {
//            "width" : 0.3825,
//                    "height" : 0.28625,
//                    "left" : 0.246667,
//                    "top" : 0.379375
//        },
//            "imageId" : "0d685812-455b-50f6-8075-72a0fda4829a",
//                "externalImageId" : "LeoJeong-studentId",
//                "confidence" : 99.9855
//        }
//        
        return new ResponseEntity<>(faces, HttpStatus.OK);
    }


    @DeleteMapping("/collections/{collectionId}/faces/{faceId}")
    public ResponseEntity<?> deleteFaceFromCollection(@PathVariable("collectionId") String collectionId, @PathVariable("faceId") String faceId) {
        DeleteFacesRequest deleteFacesRequest = new DeleteFacesRequest()
                .withCollectionId(collectionId)
                .withFaceIds(Arrays.asList(faceId));

        DeleteFacesResult deleteFacesResult = rekognitionClient.deleteFaces(deleteFacesRequest);

        List<String> faceRecords = deleteFacesResult.getDeletedFaces();
        System.out.println(Integer.toString(faceRecords.size()) + " face(s) deleted:");
        for (String face : faceRecords) {
            System.out.println("FaceID: " + face);
        }
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @PostMapping("/collections/{collectionId}/faces/searchByImage")
    public ResponseEntity<?> searchFaceMatchingImage(@PathVariable("collectionId") String collectionId, @RequestBody Map<String, Object> requestMap) {
        ObjectMapper objectMapper = new ObjectMapper();

        String photo = (String) requestMap.get("photo");
        // TODO: bucket name is hard coded. may want to pass that in later

        // Get an image object from S3 bucket.
        Image image = new Image()
                .withS3Object(new S3Object()
                        .withBucket(BUCKET)
                        .withName(photo));

        // Search collection for faces similar to the largest face in the image.
        SearchFacesByImageRequest searchFacesByImageRequest = new SearchFacesByImageRequest()
                .withCollectionId(collectionId)
                .withImage(image)
                .withFaceMatchThreshold(70F)
                .withMaxFaces(2);

        SearchFacesByImageResult searchFacesByImageResult =
                rekognitionClient.searchFacesByImage(searchFacesByImageRequest);

        System.out.println("Faces matching largest face in image from" + photo);
        List<FaceMatch> faceImageMatches = searchFacesByImageResult.getFaceMatches();
        for (FaceMatch face : faceImageMatches) {
            try {
                System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            System.out.println();
        }

//        Faces matching largest face in image frompublic/referenceImages/disney.JPG
//        {
//            "similarity" : 98.29576,
//                "face" : {
//            "faceId" : "a11f49d3-ba58-4b3c-906c-32cf24edc060",
//                    "boundingBox" : {
//                "width" : 0.476667,
//                        "height" : 0.3575,
//                        "left" : 0.24,
//                        "top" : 0.316875
//            },
//            "imageId" : "a2692ee5-8ead-52ab-b5c9-c9ed48c11216",
//                    "externalImageId" : "LeoJeong-studentId",
//                    "confidence" : 99.901596
//        }
//        }
//
//        {
//            "similarity" : 95.93436,
//                "face" : {
//            "faceId" : "fd813c43-f57d-4501-a043-5ac6648be932",
//                    "boundingBox" : {
//                "width" : 0.3825,
//                        "height" : 0.28625,
//                        "left" : 0.246667,
//                        "top" : 0.379375
//            },
//            "imageId" : "0d685812-455b-50f6-8075-72a0fda4829a",
//                    "externalImageId" : "LeoJeong-studentId",
//                    "confidence" : 99.9855
//        }
//        }

        return new ResponseEntity<>(faceImageMatches, HttpStatus.OK);
    }

    /**
     * Search the collection for a faceId?
     * Not sure what the use case here would be.
     * @param collectionId
     * @param requestMap
     * @return
     */
    @PostMapping("/collections/{collectionId}/faces/searchByFaceId")
    public ResponseEntity<?> searchFaceMatchingFaceId(@PathVariable("collectionId") String collectionId, @RequestBody Map<String, Object> requestMap) {
        ObjectMapper objectMapper = new ObjectMapper();
        // Search collection for faces matching the face id.
        String faceId = (String)requestMap.get("faceId");

        SearchFacesRequest searchFacesRequest = new SearchFacesRequest()
                .withCollectionId(collectionId)
                .withFaceId(faceId)
                .withFaceMatchThreshold(70F)
                .withMaxFaces(2);

        SearchFacesResult searchFacesByIdResult = rekognitionClient.searchFaces(searchFacesRequest);

        System.out.println("Face matching faceId " + faceId);
        List<FaceMatch> faceImageMatches = searchFacesByIdResult.getFaceMatches();
        for (FaceMatch face: faceImageMatches) {
            try {
                System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(face));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        
        return new ResponseEntity<>(faceImageMatches, HttpStatus.OK);
    }

}
