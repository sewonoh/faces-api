package com.canary.faces.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@EnableAutoConfiguration
@Api(basePath = "/hello", description = "Hello, world!")
public class HelloController {

	  private static final Logger LOGGER;

	  static {

	    LOGGER = LogManager.getLogger(HelloController.class);

	  }

	  @GetMapping("/hello")
	  @ApiOperation(value = "Default entry point",
	  		notes = "Place-holder entry point to test build configuration.")
	  public ResponseEntity<String> index() {
		  
			try {
				
				LOGGER.info("Saying hi");
				return ResponseEntity.status(HttpStatus.OK).body("Saying hi");
			
			} catch (Exception ex) {
				
				LOGGER.info("Sorry...");
				return ResponseEntity.status(HttpStatus.OK).body("Sorry...");
				
			}			  
		  
	  }

}
