package com.canary.faces.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/buckets")
public class S3BucketController {

    @Autowired
    private AmazonS3 amazonS3;
    
    // this is the S3 bucket where the reference images are stored
    private final String BUCKET = "pjeongamplifyproject8e3ab220f6f54a7ea9efecf874b92828";

    private final String LEO_REF = "public/referenceImages/leo-ref.jpg";
    private final String LEVIN_REF = "public/referenceImages/levin-ref.jpg";

    @GetMapping("/")
    public List<String> listBuckets() {
        List<String> bucketNames = new ArrayList<>();

        List<Bucket> buckets = amazonS3.listBuckets();
        System.out.println("Your Amazon S3 buckets are:");
        for (Bucket b : buckets) {
            System.out.println("* " + b.getName());
            bucketNames.add(b.getName());
        }

        return bucketNames;
    }

    @GetMapping("/{bucketId}")
    public List<String> listObjects(@PathVariable("bucketId") String bucketId) {
        List<String> objectKeys = new ArrayList<>();
        
        if (bucketId == null || bucketId.length() == 0) {
            bucketId = BUCKET;
        }

        ListObjectsV2Result result = amazonS3.listObjectsV2(bucketId);
        List<S3ObjectSummary> objects = result.getObjectSummaries();

        for (S3ObjectSummary os : objects) {
            System.out.println("* " + os.getKey());
            objectKeys.add(os.getKey());
        }

        return objectKeys;
    }
    
}
