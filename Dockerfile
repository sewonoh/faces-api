FROM openjdk:8-jdk-alpine

ARG JAR_FILE

EXPOSE 8080

VOLUME /tmp

ADD ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]